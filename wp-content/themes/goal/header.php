<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<title>
		<?php wp_title( '-', true, 'right' ); ?>
	</title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
		<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Navigation -->
<nav class="navbar navbar-inverse" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Good Game Arabia</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <?php
                    if( !is_user_logged_in() ) :
                        $login_url = wp_login_url( get_permalink() );
                        echo <<<END
                            <li>
                                <a href='{$login_url}'>LOGIN</a>
                            </li>
END;
                    else :
                        $profile_url = get_home_url().'/profile/me';
                        $logout_url  = wp_logout_url( get_permalink() );
                        $setting_url = get_home_url().'/settings';

                        $user_name   = $wp_query->current_user->display_name;
                        echo <<<END
                            <li class='dropdown'>
                                <a href='#' class="dropdown-toggle" data-toggle="dropdown"> {$user_name} <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{$profile_url}">Profile</a>
                                    </li>
                                    <li>
                                        <a href="{$setting_url}">Settings</a>
                                    </li>
                                    <li>
                                        <a href="{$logout_url}">logout</a>
                                    </li>
                                </ul>
                            </li>
END;
                    endif;
                ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
