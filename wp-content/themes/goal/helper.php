<?php  

class Helper {


	/**
	 * Helper Class
	 * @param the file name
	 * @param the directory name
	 */
	public static function load($input=null, $dir=null) {
		global $theme_path;
		
		if ($dir === null)
			$dir = $theme_path;
		else 
			$dir = $theme_path . DS . $dir;


		if (is_array($input)) {
			foreach ($input as $file) {
				require_once $dir . DS . $file . '.php';
			}
		} elseif ( preg_match('/[\w\d\-]/', $input) ) {
			require_once $dir . DS . $file . '.php';
		}
	}
}