 <?php  

session_start();
/*
 * Helpers
 */
global $theme_path;
const DS = DIRECTORY_SEPARATOR;
$theme_path = get_template_directory();
$theme_url 	= get_template_directory_uri();
set_include_path( $theme_path );

require_once $theme_path . DS . 'helper.php';
// for social data
require_once $theme_path . DS . 'community.php';

/* admin misc */
// Helper::load( array('setup', 'title', 'footer', 'manage-video-columns', 'enqueue-scripts', 'information-page'), 'admin-misc');

/* admin-ajax */
// Helper::load( array('prepare', 'get-videos', 'get-brands', 'get-chef', 'get-albums', 'get-suggests'), 'admin-ajax-handlers');

/* ajax */
Helper::load( array('prepare', 'login', 'register', 'lostpassword'), 'ajax-handlers');

/** post types */
Helper::load( array('playgrounds' ), 'post-types');

/** taxonomies */
Helper::load( array('city'), 'taxonomies');

/** Metaboxes */
Helper::load( array('extra-info'), 'metaboxes');

/** community */
// Helper::load( array('setup', 'prepare', 'redirects', 'reset-password', 'Community'), 'community');

/** misc */
Helper::load( array('title', 'redirects', 'enqueue-scripts', 'load-models', 'oauth2', 'rewrite'), 'misc');

/** Rating */
// Helper::load( array('rate'), 'rate');