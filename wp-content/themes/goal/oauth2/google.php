<?php

global $theme_path, $authUrl;
set_include_path( $theme_path . DS . 'oauth2' . DS . PATH_SEPARATOR . get_include_path());
require_once 'Google/Client.php';
require_once 'Google/Service/Plus.php';

$client_id      = '1084602166328-lhtuc6rtl2mihp7bbohllejqnjsroou6.apps.googleusercontent.com';
$client_secret  = 's0OAFX08EOo9NvD4JJIuZ3Us';
$redirect_uri   = 'http://localhost/goal/oauth2/google';

$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->setApprovalPrompt('auto');
$client->setAccessType('online');
$client->setScopes(
	'openid https://www.googleapis.com/auth/plus.login email'
);

/************************************************
	If we have a code back from the OAuth 2.0 flow,
	we need to exchange that with the authenticate()
	function. We store the resultant access token
	bundle in the session, and redirect to ourself.
 ************************************************/
if (isset($_GET['code'])) {
	$client->authenticate($_GET['code']);
	$_SESSION['google_access_token'] = $client->getAccessToken();
	//Check User or Create new 
	$redirect = get_home_url();
	// header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

/************************************************
	If we have an access token, we can make
	requests, else we generate an authentication URL.
 ************************************************/
if (isset($_SESSION['google_access_token']) && $_SESSION['google_access_token']) {
	$client->setAccessToken($_SESSION['google_access_token']);
} else {
	$authUrl = $client->createAuthUrl();
}

/************************************************
	If we're signed in we can go ahead and retrieve
	the ID token, which is part of the bundle of
	data that is exchange in the authenticate step
	- we only need to do a network call if we have
	to retrieve the Google certificate to verify it,
	and that can be cached.
 ************************************************/
if ($client->getAccessToken()) {
	$_SESSION['google_access_token'] = $client->getAccessToken();
	// $token_data = $client->verifyIdToken()->getAttributes();
	// $token_data = $client->verifyIdToken();

	$plus = new Google_Service_Plus( $client ); 
	$_me = $plus->people->get('me');

	$me  				= new stdClass;
	$me->id 			= $_me->id;
	$me->firstname 		= $_me->name->givenName;
	$me->lastname		= $_me->name->familyName;
	$me->birthdate 		= $_me->birthday;

	$me->gender 		= $_me->gender;
	$me->aboutMe 		= $_me->aboutMe;
	$me->email 			= $_me->emails[0]->value;
	$me->image 			= substr(
		$_me->image->url, 
		0, 
		stripos($_me->image->url, '?')
	);
	$me->occupation 			= $_me->occupation;
	$me->google_access_token 	= $client->getAccessToken();

	if ( is_user_logged_in() ) {

		$_SESSION['api-integration-data'] = array(
			'api-id' 	=> $me->id,
			'api-type' 	=> 'google',
			'api-data' 	=> $me,
			'wp-id'		=> get_current_user_id(),
		);

		wp_redirect( get_home_url() . '/oauth2/integrate?nonce=' . wp_create_nonce('integrate') );
		exit;
		
	} else
		Community::google( $me );
}