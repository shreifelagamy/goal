<?php 
	
global $theme_path;
set_include_path( $theme_path . DS . 'oauth2' . DS . PATH_SEPARATOR . get_include_path());
require_once "Facebook/autoload.php";
use Facebook\FacebookRequest;
use Facebook\GraphUser;

$id 			= '803387033054873';
$secret 		= '416239b22f2cf84f7862c7ee210be889';
$redirect_uri	= 'http://localhost/goal/oauth2/facebook';

Facebook\FacebookSession::setDefaultApplication($id, $secret);
$helper = new Facebook\FacebookRedirectLoginHelper($redirect_uri);

try {
    $session = $helper->getSessionFromRedirect();
} catch(Facebook\FacebookRequestException $ex) {
    // When Facebook returns an error
} catch(\Exception $ex) {
    // When validation fails or other local issues
}

if ( isset($session) ) {
	$_SESSION['token'] = $session->getToken();
	$request = new FacebookRequest(
		$session, 
		'GET', 
		'/me'
	);
	$response 		= $request->execute();
	$graphObject 	= $response->getGraphObject(GraphUser::className());

	$me  					= new stdClass;
	$me->id 				= $graphObject->getId();
	$me->firstname 			= $graphObject->getFirstName();
	$me->lastname 			= $graphObject->getLastName();
	$me->username 			= $graphObject->getName();
	$me->email 				= $graphObject->getProperty('email');
	$me->birthdate			= $graphObject->getProperty('birthday');
	$me->gender 			= $graphObject->getProperty('gender');

	$request = new FacebookRequest(
		$session, 
		'GET', 
		'/me/picture',
		 array (
		    'redirect' => false,
		    'height' => '200',
		    'type' => 'normal',
		    'width' => '200',
		)
	);

	$response 		= $request->execute();
	$graphObject 	= $response->getGraphObject();

	$me->image  				= $graphObject->getProperty('url');
	$me->facebook_access_token	= $session->getToken();
	
	$session = null;
	if ( is_user_logged_in() ) {
		
		$_SESSION['api-integration-data'] = array(
			'api-id' 	=> $me->id,
			'api-type' 	=> 'facebook',
			'api-data' 	=> $me,
			'wp-id'		=> get_current_user_id(),
		);

		wp_redirect( get_home_url(). '/oauth2/integrate?nonce=' . wp_create_nonce('integrate') );
		exit;
	} else
		Community::facebook($me);
} else {
	echo 'error occured!';
}