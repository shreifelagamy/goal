<?php
/**
 * @file
 * User has successfully authenticated with Twitter. Access tokens saved to session and DB.
 */

/* Load required lib files. */
require_once 'Twitter/config.php';
require_once('Twitter/twitteroauth.php');


/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

/* Request access tokens from twitter */

$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

/* Save the access tokens. Normally these would be saved in a database for future use. */
$_SESSION['access_token'] = $access_token;

/* Remove no longer needed request tokens */
unset($_SESSION['oauth_token']);
unset($_SESSION['oauth_token_secret']);
unset($_SESSION['twitterLoginRedirectURL']);

/* If HTTP response is 200 continue otherwise send to connect page to retry */
if ( $connection->http_code === 200) {
	$_SESSION['status'] = 'verified';

	/* If access tokens are not available redirect to connect page. */
	if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
	    header('Location: ./Twitter/clearsessions.php');
	}
	/* Get user access tokens out of the session. */
	$access_token = $_SESSION['access_token'];

	/* Create a TwitterOauth object with consumer/user tokens. */
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

	/* If method is set change API call made. Test is called by default. */
	$content = $connection->get('account/verify_credentials');

	$me                 		= new stdClass;
	$me->id  		       		= $content->id;
	$me->username       		= $content->name;
	$me->screen_name    		= $content->screen_name;
	$me->aboutMe 				= $content->description;
	// $me->location       		= $content->location;
	$me->img_url        		= $content->profile_image_url;
	$me->twitter_access_token   = $access_token['oauth_token'];
	// get the full image
	$me->image        		= str_replace('_normal','',$me->img_url);


	if ( is_user_logged_in() ) {

		$_SESSION['api-integration-data'] = array(
			'api-id' 	=> $me->id,
			'api-type' 	=> 'twitter',
			'api-data'	=> $me,
			'wp-id'		=> get_current_user_id(),
		);

		wp_redirect( get_home_url() . '/oauth2/integrate?nonce=' . wp_create_nonce('integrate') );
		exit;
		
	} else
		Community::twitter($me);

	// $me->email          = $content->getProperty('email');
	// $me->birthday       = $content->getProperty('birthday');


	/* Some example calls */
	//$connection->get('users/show', array('screen_name' => 'abraham'));
	//$connection->post('statuses/update', array('status' => date(DATE_RFC822)));
	//$connection->post('statuses/destroy', array('id' => 5437877770));
	//$connection->post('friendships/create', array('id' => 9436992));
	//$connection->post('friendships/destroy', array('id' => 9436992));

	/* Include HTML to display on the page */
	// include('html.inc');
	/* The user has been verified and the access tokens can be saved for future use */
} else {
  /* Save HTTP status for error dialog on connnect page.*/
  unset( $_SESSION['access_token'] );
  echo 'please try again';
  exit;
}
