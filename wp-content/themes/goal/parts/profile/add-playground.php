<form class='publish-form' method="post" enctype="multipart/form-data" action="<?php echo get_home_url().'/oauth2/addpost' ?>" >
	<h2>Add Court</h2>

	<!-- Warning Messages -->
	<?php
		if( isset($_SESSION['message']) && !empty($_SESSION['message']) ) {
			echo "<div class='warnings alert alert-".$_SESSION['message']['type']."'><ul>";
			foreach ($_SESSION['message']['data'] as $key => $value) {
				echo "<li>".$value."</li>";
			}
			echo "</ul></div>";
			unset($_SESSION['message']);
		} else {
			echo "<div class='warnings alert hidden'></div>";
		}
	?>

	<div class="form-group">
		<label for="title">Title</label>	
		<input 
			type="text" 
			id="title" 
			class="form-control" 
			name="post-header" 
			placeholder='Enter title here ...'
		>
	</div>

	<div class="form-group">
		<label for="description">Description</label>	
		<textarea name="post-content" class="form-control" id="description" rows="5" placeholder="Enter your description"></textarea>
	</div>

	<div class="form-group">
		<label for="space">Space</label>	
	  	<input type="number" id="space" class="form-control" name="post-space" placeholder='Enter space here ...' onkeypress = 'return event.charCode >= 48 && event.charCode <= 57'>
	</div>

	<div class="form-group">
		<label for="price">Price</label>
		<div class="input-group">
			<div class="input-group-addon">$</div>
			<input type="text" name="post-price" class="form-control" id="exampleInputAmount" placeholder="Amount">
			<div class="input-group-addon">.00</div>
		</div>	
	</div>

	<div class="form-group">
		<label for="players_num">Number of players</label>	
		<input type="number" id="players_num" class="form-control" name="post-players-num" placeholder='Enter Players Number ...' onkeypress = 'return event.charCode >= 48 && event.charCode <= 57'>
	</div>

	<div class="form-group">
		<label for="phone_num">Phone Number</label>	
		<input type="number" id="phone_num" class="form-control" name="phone_num" placeholder='Enter Phone number ...' onkeypress = 'return event.charCode >= 48 && event.charCode <= 57'>
	</div>

	<?php if( isset( $wp_query->court_terms ) && !empty($wp_query->court_terms) ) : ?>
	<div class="form-group">
		<label for="city">Choose City</label>	
		<select class="form-control" name="post-cat">
			<option value="">Choose City</option>
		<?php 
			foreach ($wp_query->court_terms as $key => $term) : 
				echo "<option value='".$term->term_id."'>".$term->name."</option>";
		 	endforeach;
		?>
		</select>
	</div>
	<?php endif; ?>

	<div class='form-group'>
	  <label for='image'>Add Image</label>
	  <div class="input-group">
	      <input id="image" class='form-control image-upload' name="image-upload">
	      <div class='input-group-addon upload'></div>
	  </div>
	  <input type="file" id='upload-file' name="upload-file" style="display:none;">
	</div>

	<button type="submit" class="btn btn-primary">Submit</button>

	<?php wp_nonce_field( 'addpost' ); ?>
</form>