(function ($, w) {

	var RegisterPage = {
		init: function () {
			RegisterPage.prepare();
			RegisterPage.events();
		},

		prepare: function () {
			// underscore for errors
			RegisterPage.messageTemplate 	= $('#messages').html();

			RegisterPage.form 				= $('form.form-register');
			RegisterPage.submitButton 		= RegisterPage.form.find('[type=submit]');

			RegisterPage.message 			= RegisterPage.form.find('div.warnings');
		},

		events: function () {
			if( RegisterPage.message.hasClass('alert-danger') ) {
				RegisterPage.form.find('.form-group').addClass('has-error');
			}
			
			RegisterPage.form.submit(function (e) {
				e.preventDefault();

				RegisterPage.data = {
					action: 'register_action',
					inputs:	$(this).serialize(),
				};

				RegisterPage.form.find('input').prop('disabled', true);

				var firstCaption = RegisterPage.submitButton.val();
				RegisterPage.submitButton.val('Validate inputs');

				// var username = RegisterPage.form.find('input[name=username]');

				RegisterPage.loginAction(firstCaption);
			});
		},

		validateForm : function ( form ) {
			// RegisterPage.form.find('input').each(function(i) {
			// 	if( !$(this).val() ){
			// 		$(this).parent().addClass('has-error');
			// 		$(this).effect("shake");
			// 	}
			// });

			// var email 		= RegisterPage.form.find('input[name=email]').val();
			// if( !isValidEmailAddress(email) ) {
			// 	RegisterPage.form.find('input[name=email]').addClass('has-error');
			// }

			// var username 	= RegisterPage.form.find('input[name=username]').val();
		},

		isValidEmailAddress : function ( email ) {

			// var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		 //    return pattern.test(email);
		},

		loginAction: function (firstCaption) {
			$.ajax({
				url: w.ajaxurl,
				data: RegisterPage.data,
				type: 'POST',
			}).done(function (response) {

				if ( response.success == false ) {

					RegisterPage.message.removeClass('hidden');

					if( !RegisterPage.message.hasClass('alert-danger') )
						RegisterPage.message.addClass('alert-danger');	

					RegisterPage.message.html('<ul>'+
						_.template( RegisterPage.messageTemplate, {
								messages: response.data
							}
						)+'</ul>'
					);
					
					RegisterPage.form.find('input').prop('disabled', false);

					if( !RegisterPage.form.find('div.form-group').hasClass('has-error') ) 
						RegisterPage.form.find('div.form-group').addClass('has-error');

					RegisterPage.submitButton.val(firstCaption);

				} else {
					RegisterPage.message.removeClass('hidden');

					if( RegisterPage.message.hasClass('alert-danger') )
						RegisterPage.message.removeClass('alert-danger');	

					RegisterPage.message.addClass('alert-success');

					RegisterPage.message.html('<ul>'+
						_.template( RegisterPage.messageTemplate, {
								messages: response.data
							}
						)+'</ul>'
					);

					window.setTimeout(function() {
						w.location.href = w.homepageurl;
					}, 2000);
				}

			});
		}
	}

	$(document).ready(function () {
		RegisterPage.init();
	});

}(jQuery, window));