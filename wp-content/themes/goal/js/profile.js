(function (w, $) {
	var AddPost = {
		init: function () {
			AddPost.prepare();
			AddPost.events();
		},

		prepare: function () {
			AddPost.communityTemplate 			= $('#messages').html();
			// AddPost.communityMessageContainer 	= $('div.form-group.message');

			AddPost.fileInput 					= $('input[type="file"]');
			AddPost.inputImageInput				= $('input.image-upload');
		},

		events: function () {

			$('div.upload').on("click", function(e){
				e.preventDefault();
				AddPost.fileInput.trigger('click');

				AddPost.fileInput.change(function() {
			        var filename = AddPost.fileInput.val();
			        AddPost.inputImageInput.val(filename);
			        AddPost.inputImageInput.prop('disabled', true);
			    });
			});

			// $('form.publish-form').submit(function(e) {
			// 	if( !$(this).attr('validated') ){
			// 		e.preventDefault();
			// 		var header 	= $('input[name=post-header]').val();
			// 		var content = $('textarea[name=post-content]').val();
			// 		var cat 	= $('select[name=post-cat]').val();
			// 		var image   = AddPost.inputImageInput.val();
			// 		var nonce 	= $('#_wpnonce').val();

			// 		AddPost.data.header 	= header;
			// 		AddPost.data.content 	= content;
			// 		AddPost.data.cat 		= cat;
			// 		AddPost.data.image 		= image;
			// 		AddPost.data.nonce 		= nonce;

			// 		AddPost.ajaxAction();

			// 		return false;
			// 	}

			// 	return true;
			// });
		},

		ajaxAction: function () {

			$.ajax({
				url: w.ajaxurl,
				data: AddPost.data,
				type: 'POST'
			}).done(function (response) {
				
				if( response.success === true ) {
					$('form.publish-form').attr('validated', true);
					$('form.publish-form [type=submit]').trigger('click');
					// $('form.publish-form').trigger('submit');
					
					// if( AddPost.communityMessageContainer.hasClass('alert alert-danger') ) {
					// 	AddPost.communityMessageContainer.removeClass('alert alert-danger');
					// }

					// AddPost.communityMessageContainer.addClass('alert alert-success');
					// AddPost.communityMessageContainer.html('<ul>'+
					// 	_.template(
					// 		AddPost.communityTemplate,
					// 		{
					// 			messages: response.data
					// 		}
					// 	)
					// +'</ul>');
				} else {
					if( response.data == false ) {
						$('#login-modal').modal();
					} else {
						AddPost.communityMessageContainer.addClass('alert alert-danger');
						AddPost.communityMessageContainer.html('<ul>'+
							_.template(
								AddPost.communityTemplate,
								{
									messages: response.data
								}
							)
						+'</ul>');
					}
				}
			});
		}
	}

	$(document).ready(function (){
		AddPost.init();
	});
}(window, jQuery));