(function ($, w) {

	var LoginPage = {
		init: function () {
			LoginPage.prepare();
			LoginPage.events();
		},

		prepare: function () {
			// underscore for errors
			LoginPage.messageTemplate 	= $('#message').html();

			LoginPage.form 				= $('form.form-signin');
			LoginPage.submitButton 		= LoginPage.form.find('[type=submit]');
			// inputs
			LoginPage.form.user 		= LoginPage.form.find('input[name=user]');
			LoginPage.form.password 	= LoginPage.form.find('input[name=password]');
			LoginPage.form.nonce 		= LoginPage.form.find('#_wpnonce');
			LoginPage.form.rememberme 	= LoginPage.form.find('input[type=checkbox]');

			LoginPage.message 			= LoginPage.form.find('div.warnings');
		},

		events: function () {
			if( LoginPage.message.hasClass('alert-danger') ) {
				LoginPage.form.find('.form-group').addClass('has-error');
			}
			
			LoginPage.form.submit(function (e) {
				e.preventDefault();
				
				LoginPage.form.find('input').prop('disabled', true);

				var firstCaption = LoginPage.submitButton.val();
				LoginPage.submitButton.val('Validate inputs');

				LoginPage.data = {
					action: 'login_action',
					inputs: {
						user: 		LoginPage.form.user.val(),
						password: 	LoginPage.form.password.val(),
						rememberme: LoginPage.form.rememberme.prop('checked'),
						nonce: 		LoginPage.form.nonce.val()
					},
				};

				LoginPage.loginAction(firstCaption);
			});
		},

		loginAction: function (firstCaption) {
			$.ajax({
				url: w.ajaxurl,
				data: LoginPage.data,
				type: 'POST',
			}).done(function (response) {

				if ( response.success == false ) {

					if( LoginPage.message.hasClass('hidden') )
						LoginPage.message.removeClass('hidden');

					if( LoginPage.message.hasClass('alert-success') )
						LoginPage.message.removeClass('alert-success');

					if( !LoginPage.message.hasClass('alert-danger') )
						LoginPage.message.addClass('alert-danger');	

					LoginPage.message.html('<ul>'+
						_.template( LoginPage.messageTemplate, {
								message: response.data
							}
						)+'</ul>'
					);
					
					LoginPage.form.find('input').prop('disabled', false);
					LoginPage.submitButton.val(firstCaption);

					LoginPage.form.find('.login-input div.col-sm-6').addClass('has-error');

				} else {
					w.location.href = w.homepageurl;
				}

			});
		}
	}

	$(document).ready(function () {
		LoginPage.init();
	});

}(jQuery, window));