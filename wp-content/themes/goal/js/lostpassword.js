(function (w, $) {
	var LostPassword = {
		init: function () {
			LostPassword.prepare();
			LostPassword.events();
		},

		prepare: function () {
			LostPassword.messageTemplate 			= $('#message').html();

			LostPassword.form 				= $('form.form-lostpassword');
			LostPassword.messageContainer 	= LostPassword.form.find('.warnings');
			LostPassword.userInput	 		= LostPassword.form.find('input[name=user]');
			LostPassword.nonceInput	 		= LostPassword.form.find('#_wpnonce');
			LostPassword.submitButton		= LostPassword.form.find('button[type=submit]');
		},

		events: function () {

			// for recipes
			LostPassword.form.submit( function(e) {
				e.preventDefault();

				LostPassword.form.find('input').prop('disabled', true);

				// LostPassword.submitButton.prop('disabled', true);
				var firstCaption = LostPassword.submitButton.text();
				LostPassword.submitButton.text('Validating');

				LostPassword.data = {
					action: 'lost_password',
					user: LostPassword.userInput.val(),
					nonce: LostPassword.nonceInput.val()
				};

				LostPassword.ajaxAction(firstCaption);
			});
		},

		ajaxAction: function (firstCaption) {
			$.ajax({
				url: w.ajaxurl,
				data: LostPassword.data,
				type: 'POST'
			}).done(function (response) {

				if( response.success == true ) {
					if( LostPassword.form.find('div.form-group').hasClass('has-error') ) 
						LostPassword.form.find('div.form-group').removeClass('has-error');

					if( LostPassword.messageContainer.hasClass('alert-danger') ) 
						LostPassword.messageContainer.removeClass('alert-danger');

					LostPassword.messageContainer.addClass('alert-success');

					LostPassword.messageContainer.html('<ul>'+
						_.template( LostPassword.messageTemplate, {
								message: response.data
							}
						)+'</ul>'
					);

					LostPassword.submitButton.text(firstCaption);

					window.setTimeout(function() {
						w.location.href = w.homepageurl;
					}, 2000);

				} else {

					if( !LostPassword.form.find('div.form-group').hasClass('has-error') ) 
						LostPassword.form.find('div.form-group').addClass('has-error');

					if( LostPassword.messageContainer.hasClass('alert-success') ) 
						LostPassword.messageContainer.removeClass('alert-success');

					if( !LostPassword.messageContainer.hasClass('alert-danger') )
						LostPassword.messageContainer.addClass('alert-danger');

					LostPassword.messageContainer.removeClass('hidden');
					LostPassword.messageContainer.html('<ul>'+
						_.template(
							LostPassword.messageTemplate, 
							{
								message: response.data
							}
						)+'</ul>'
					);

					LostPassword.form.find('input').prop('disabled', false);
					LostPassword.submitButton.text(firstCaption);
				}
			});
		}
	}

	$(document).ready(function (){
		LostPassword.init();
	});
}(window, jQuery));