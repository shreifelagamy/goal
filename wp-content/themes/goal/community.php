<?php

class Community {

	private $actionType, $api, $data, $wpdb;

	private function __construct($data=null, $api=null) {
		global $wpdb;
		$this->wpdb = $wpdb;

		if ( $data != false ) {
			$this->api 	= $api;
			$this->data = $data;

			// $this->_prepareData();
			$this->_checkActionType();		
		}
	}

	// private function _prepareData() {
	// 	$requiredInputs = array(
	// 		'firstname', 'lastname', 'id', 'birthdate', 'gender', 'data'
	// 	);

	// 	foreach ($requiredInputs as $key) {
	// 		if ( empty($this->data->$key) )
	// 			$this->data->$key = null;
	// 	}

	// 	if ( empty($this->data->firstname) && empty($this->data->lastname) && $this->api === 'twitter' )
	// 		$this->data->firstname = $this->data->username;

	// }

	private function _checkActionType () {
		$userID = $this->_getUserID();
		if ( !!$userID === true ) {
			$this->_login( $userID );
		} else {
			$this->_register( $this->data );
		}

	}

	private function _login ($userID) {

		$_SESSION['notice'] = $userID->user_login;

		wp_redirect( home_url().'/login' );
		exit;
		// $user = get_user_by('id', $userID);
		// if( $user ) {
		// 	wp_set_current_user( $userID, $user->user_login );
		// 	wp_set_auth_cookie( $userID );
		// 	do_action( 'wp_login', $user->user_login );
		// 	wp_redirect( get_home_url() );
		// 	exit;
		// }
	}

	private function _register( $user ) {

		$_SESSION['user'] = $user;

		wp_redirect( home_url().'/register' );
		exit;
		// if ( email_exists($this->data->email) ) {
		// 	$user = get_user_by( 'email', $this->data->email );
		// 	$_SESSION['api-integration-data'] = array(
		// 		'api-id' 	=> $this->data->id,
		// 		'wp-id'		=> $user->ID,
		// 		'api-type'	=> $this->api,
		// 		'api-data'	=> $this->data,
		// 	);

		// 	wp_redirect( get_home_url() . '/oauth2/ask?nonce='. wp_create_nonce('ask') );
		// 	exit;
		// }
		
		// $userdata = array(
		// 	'user_email' 	=> $this->data->email,
		// 	'user_nicename'	=> "{$this->data->firstname}",
		// 	'user_login'	=> "{$this->api}_{$this->data->id}",
		// 	'display_name' 	=> "{$this->data->firstname} {$this->data->lastname}",
		// 	'first_name'	=> $this->data->firstname,
		// 	'last_name'		=> $this->data->lastname,
		// 	'user_pass'		=> wp_generate_password( 20, true ),
		// );

		// $userID = wp_insert_user( $userdata );
		
		// if( is_wp_error($userID) ) {
		// 	echo __('unknown error occured!', 'shamlola');
		// 	exit;
		// }

		// update_user_meta( $userID, "{$this->api}_id", $this->data->id );
		
		// update_user_meta( 
		// 	$userID, 
		// 	'profile',  
		// 	array(
		// 		'gender' 	=> $this->data->gender ?: null,
		// 		'birthdate' => $this->data->birthdate ?: null,
		// 	)
		// );

		// update_user_meta( 
		// 	$userID, 
		// 	"{$this->api}_profile", 
		// 	$this->data
		// );


		// $this->_login($userID);
	}

	private function _getUserID($meta_key=null, $meta_value=null) {

		return get_user_by( 'email', $this->data->email );

		// $meta_key 	= $meta_key 	?: "{$this->api}_id";
		// $meta_value = $meta_value 	?: "{$this->data->id}";
		
		// return $this->wpdb->get_var(
		// 	"
		// 		SELECT user_id FROM {$this->wpdb->usermeta} 
		// 			WHERE 	meta_key 	= '{$meta_key}' AND
		// 					meta_value 	= '{$meta_value}'
		// 	"
		// ) ?: null;
	}

	public static function getUserID ($meta_key, $meta_value)  {
		global $wpdb;
		$self = new self();
		return $self->_getUserID($meta_key, $meta_value);
	}

	public static function __callStatic( $method, $args=array() ) {
	
		$apiSupported = in_array(
			$method,
			array( 'facebook', 'google', 'twitter' )
		);

		if ( !$apiSupported ) {
			throw new Exception("we don't support this library", 1);
			exit;
		}

		if ( !isset($args[0]) ) {
			throw new Exception("Damn it, dude. you missed the arg", 1);
			exit;
		}

		$self = new self( $args[0], $method );
	}	

}