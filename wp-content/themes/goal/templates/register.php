<?php  
	/*
	Template Name: Register
	*/

  get_header();

  if( isset($_SESSION['user']) ) {
    $firstname = $_SESSION['user']->firstname;
    $lastname  = $_SESSION['user']->lastname;
    $email     = $_SESSION['user']->email;
  }

?>
<div class='container'>
  <form class="form-register">

      <div class='header'>
        <h1>
          Registeration
        </h1>
        <p>
          Are you sure You wanna do this ?!
        </p>
      </div>

      <!-- Warning Messages -->
      <div class='warnings alert hidden'>
        <ul>
          <li>
            focus in entering data
          </li>
        </ul>
      </div>

      <!-- Register Form -->
      <div class='form-group'>
        <label for="firstname"> Firstname </label>
        <input 
          required    = 'required'  
          class       = 'form-control'  
          name        = 'firstname' 
          id          = 'firstname' 
          value       = "<?php echo isset($firstname) ? $firstname : ''  ?>"
          autofocus
        >
      </div>

      <div class='form-group'>
        <label for="lastname"> Lastname </label>
        <input 
          required    = 'required'  
          name        = 'lastname' 
          id          = 'lastname' 
          class       = 'form-control'
          value       = "<?php echo isset($lastname) ? $lastname : ''  ?>"
        >
      </div>

      <div class='form-group'>
        <label for="username"> Username </label>
        <input required='required'  name='username' id='username' class='form-control'>
      </div>

      <div class='form-group'>
        <label for="email"> E-mail address </label>
        <input 
          required    = 'required'  
          type        = 'email' 
          name        = 'email' 
          id          = 'email' 
          class       = 'form-control'
          value       = "<?php echo isset($email) ? $email : '' ?>"
          <?php echo (isset($email) && !empty($email)) ? 'readonly' : ''; ?> 
        >
      </div>

      <div class='form-group'>
        <label for="password"> Password </label>
        <input required='required' class='form-control'  name='password' id='password' type='password'>
      </div>

      <div class='form-group'>
        <label for="password-confirmation"> Confirm Password </label>
        <input required='required' class='form-control' type='password'  name='password-confirmation' id='password-confirmation'>
      </div>

        <a class="btn link" href="<?php echo get_home_url().'/login'; ?>"> Login </a>
        <a class='btn link' href='<?php echo get_home_url() . '/lostpassword'; ?>'>Lost</a>
      <input class='submit btn btn-lg pull-right btn-primary' type='submit' value='Register'>

      <?php wp_nonce_field( 'register' ); ?>
  </form>
</div>

<?php get_footer(); ?>