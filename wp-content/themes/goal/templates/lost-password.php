<?php  
	/*
	 Template Name: Lost Password
	*/

  get_header();
?>
	<div class='container'>
    <form class='form-lostpassword'>

      <h2 class="form-lostpassword-heading"><?php the_title(); ?></h2>

      <!-- warning messages -->
      <div class="alert warnings hidden"></div>
          
      <!-- Lost password form -->
      <div class='form-group'>
        <label for="user">Username / Email address</label>
        <input class='form-control' id='user' required='required' name='user' placeholder='Enter Username / emaill address'>
      </div>

        <a class="btn link" href="<?php echo get_home_url().'/login'; ?>"> Login </a>
        <a class="btn link" href="<?php echo get_home_url().'/register'; ?>"> Register </a>
        <button type="submit" class="btn btn-lg btn-danger pull-right" > Submit </button>

      <?php wp_nonce_field( 'lostpassword' ); ?>

    </form>
  </div>
<?php get_footer(); ?>