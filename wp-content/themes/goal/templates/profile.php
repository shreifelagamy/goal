<?php  
  /*
  Template Name: Profile
  */

  get_header();
?>
<div class='container'>

	<!-- title section -->
  <div class="row">
		<div class="col-lg-12">
      <h1 class="page-header">Profile Playground....
      </h1>
      <ol class="breadcrumb">
          <li><a href="index.html">Home</a>
          </li>
          <li class="active">Blog Home One</li>
      </ol>
    </div>
  </div>

  <!-- Content sections -->
	<div class="row">
		<div class="col-md-8">
			<!-- add playground form -->
			<?php include locate_template( 'parts/profile/add-playground.php' ); ?>
			<hr>
			<!-- current playgorunds -->
			<?php include locate_template( 'parts/profile/my-playgrounds.php' ); ?>
		</div>

		<!-- Sidebar -->
		<?php get_sidebar(); ?>
    
	</div>


</div>
<?php get_footer(); ?>