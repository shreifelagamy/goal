<?php  
	/*
	Template Name: Login
	*/
  get_header(); 

  $class  = 'hidden';
  $notice = '';
  if( isset($_SESSION['notice']) ) {
    $class  = 'alert-success';
    $notice = "this Username <b>".$_SESSION['notice']."</b> already exist <b><a href='".get_home_url()."/lostpassword'> LOST PASSWORD ? </a></b>"; 
  }

  global $facebook_authUrl;
?>
  <div class="container">

    <form class="form-signin">

      <h2 class="form-signin-heading">Please sign in</h2>

      <!-- warning messages -->
      <div class="alert warnings <?php echo $class ?>"><?php echo $notice ?></div>

      <div class="form-group">
        <label for="inputEmail" class="sr-only">Email address</label> 
        <input type="text" name='user' id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus=""> 
      </div>

      <div class="form-group">
        <label for="inputPassword" class="sr-only">Password</label> 
        <input type="password" name='password' id="inputPassword" class="form-control" placeholder="Password" required="">
      </div>

      <div class="checkbox">
        <label><input type="checkbox" name="remember-me" value="remember-me"> Remember me </label>
      </div>

      <?php wp_nonce_field( 'signin' ); ?>
      
      <!-- social networks -->
      <div class='using-networks'>
        <a class='facebook' href='<?php echo $facebook_authUrl; ?>'></a>
        <!-- <a class='twitter' href='<?php echo $twitter_authUrl; ?>'></a> -->
        <a class='google' href='<?php echo $google_authUrl; ?>'></a>
        <div class='clearfix'></div>
      </div>
      
        <a class='btn link' href='<?php echo get_home_url() . '/lostpassword'; ?>'>Lost</a>
        <a class="btn link" href="<?php echo get_home_url().'/register'; ?>"> Register </a>
      <button class="btn btn-primary btn-lg pull-right" type="submit">Sign in</button>

    </form>
  </div>
<?php get_footer(); ?>