<?php  
  /*
    Template Name: Reset Password
  */

  if( !isset($_SESSION['user']) && empty($_SESSION['user']) ) {
    wp_redirect( home_url() );
    exit;
  }

  $notice           = '<b>'.$_SESSION['user']->user_nicename.'</b> انت الان مصرح لك تغير كلمه المرور. و إن لم يكن هذا انت الرجاء العوده الى الصفحه <b><a href="'.get_home_url().'">الرئيسيه</a></b>';
  $activation_code  = $_SESSION['user']->user_activation_key;
  
  // unset($_SESSION['user']);

  get_header(); 
?>
	<div class='container'>
    <form class='form-resetpassword'>

      <h1> Reset Your Password </h1>

      <!-- notice message -->
      <div class='notice alert alert-success'>
        <ul>
          <li><?php echo $notice; ?></li>
        </ul>
      </div>

      <!-- Error warning -->
      <div class='warnings alert alert-danger hidden'></div>
          
      <!-- login form -->
      <div class='form-group'>
        <label for="password"> Password </label>
        <input type="password" class='form-control' id="password" required='required' name='password' placeholder='Enter Your password here '>
      </div>

      <div class='form-group'>
        <label for="confirm-password"> Confirm Password </label>
        <input type='password' class='form-control' required='required' id="confirm-password" name='confirm-password' placeholder='Enter password confiration here'>
      </div>

      <div class="btn-group">
        <button class='btn btn-lg btn-primary' type='submit'>Submit</button>
      </div>

      <?php wp_nonce_field( 'resetpassword' ); ?>
      <input type='hidden' name='activation-code' value='<?php echo $activation_code; ?>'>

    </form>
  </div>
<?php get_footer(); ?>