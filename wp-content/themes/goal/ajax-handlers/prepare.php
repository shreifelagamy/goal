<?php

function append_ajaxurl_to_header() {
	?>
	<script type="text/javascript">
	(function (w) {
		w.ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
		w.homepageurl = "<?php echo get_home_url(); ?>";
	}(window));
	</script>
	<?php
}

add_action('wp_head', 'append_ajaxurl_to_header');
add_action('admin_head', 'append_ajaxurl_to_header');