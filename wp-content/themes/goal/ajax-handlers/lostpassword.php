<?php

function lost_password () {

	$nonce = wp_verify_nonce( $_REQUEST['nonce'], 'lostpassword' );
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] && $nonce === 1 ) {

		global $error_array;
		$error_array = array();
		
		if( empty($_POST['user']) ) $error_array[] = 'Please Enter username <b>OR</b> Email address';

		if( count($error_array) > 0 ) {
			wp_send_json_error( $error_array );
			exit;
		}

		if ( is_email($_POST['user']) )
			$user = get_user_by( 'email', $_POST['user'] );
		else
			$user = get_user_by( 'login', $_POST['user'] );

		if ( $user == null ) {
			$error_array[] = is_email($_POST['user']) ? 'This email address doesnot exist.' : 'this username doesnot exist.';
			wp_send_json_error( $error_array );
			exit;
		} else {
			global $wpdb;

			$user_activation_key = $wpdb->get_var(
				$wpdb->prepare("SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user->user_login)
			);

			if ( null == $user_activation_key ) {
				$user_activation_key = wp_generate_password( 20, false );
				$wpdb->update( 
					$wpdb->users,   
					array(
						'user_activation_key' 	=> $user_activation_key
					),
					array(
						'user_login' 			=> $user->user_login
					)
				);
			}

			$msg_nonce = wp_create_nonce( 'resetpassword' );

			$reset_link 	= get_home_url() . '/login/?action=lostpassword&user_activation_key=' . $user_activation_key.'&nonce='. $msg_nonce;
			$website_title 	= get_option('blogname');

			$message = <<<EOT
			عزيزي $user->user_login \n\r
			لقد تلقينا طلبا لإعادة تعيين كلمة المرور لحساب $website_title الخاص بك \n\r
			إذا كنت ترغب في إعادة تعيين كلمة المرور الخاصة بك، انقر على الرابط أدناه (أو نسخ ولصق الرابط في المتصفح الخاص بك):
			$reset_link \n\r
			إذا كنت لا تريد إعادة تعيين كلمة المرور الخاصة بك، يرجى تجاهل هذه الرسالة. \n\r\n\r

			فريق $website_title.
EOT;

			$header = "من: {$website_title} الدعم <" . get_option('admin_email') . "> ";
			wp_mail( $user->user_email, 'إعاده تعيين كلمه المرور', $message, $header );		

			wp_send_json_success( 'سوف تتلقى رسالة على بريد إلكتروني في غضون دقائق قليلة مع الإرشادات لإعادة تعيين كلمة المرور الخاصة بك. <br> إن لم تصلك رساله يرجى المحاوله مره اخرى.' );
			exit;
		}
	} else {
		wp_send_json_error( 'Who are you mother fucker !' );
		exit;
	}
}

add_action('wp_ajax_lost_password', 			'lost_password');
add_action('wp_ajax_nopriv_lost_password', 		'lost_password');