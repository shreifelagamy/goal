<?php

function login_action() {

	$nonce = wp_verify_nonce( $_POST['inputs']['nonce'], 'signin' );
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] && $nonce === 1 ) {
		global $error_array;
		$error_array = array();

		$user 		= $_POST['inputs']['user'];
		$password 	= $_POST['inputs']['password'];
		$remember 	= $_POST['inputs']['rememberme'];

		if( empty($user) ) 			$error_array[] = 'Enter username';
		elseif( empty($password) ) 	$error_array[] = 'يرجى إدخال كلمه السر.';

		if( count($error_array) > 0 ) {
			wp_send_json_error( $error_array );
			exit;
		}

		// lets signin the user
		if ( is_email($user) ) {
			global $wpdb;
			$user = $wpdb->get_var(
				$wpdb->prepare(
					"SELECT user_login FROM {$wpdb->users} WHERE user_email='%s'",
					array( $user )
				)
			);
		}

		$creds = array();
		$creds['user_login'] 	= $user;
		$creds['user_password'] = $password;
		$creds['remember'] 		= $remember;
		// here is the sign in
		$user = wp_signon( $creds, false );

		if ( is_wp_error($user) ) {
			$error_array[] = $user->get_error_message();
			wp_send_json_error( $error_array );
			exit;

		} else {
			if( isset($_SESSION['notice']) ) unset($_SESSION['notice']);
			var_dump($user);
			die;
			// for remember me 
			wp_set_auth_cookie( $user_id, $remember, $secure );
			
			$redirect = get_home_url();
			wp_send_json_success($redirect);
			exit;
		}

	} else {
		wp_send_json_error( 'Who are you mother fucker !' );
		exit;
	}
}

add_action('wp_ajax_login_action', 			'login_action');
add_action('wp_ajax_nopriv_login_action', 	'login_action');