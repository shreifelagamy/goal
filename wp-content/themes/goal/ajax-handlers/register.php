<?php

function register_action() {

	parse_str($_POST['inputs'], $inputs);

	$nonce = wp_verify_nonce( $inputs['_wpnonce'], 'register' );
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] && $nonce === 1 ) {
		global $error_array;
		$error_array = array();

		foreach( array('firstname', 'lastname', 'username', 'email', 'password', 'password-confirmation') as $key ) {

			if ( !isset($inputs[$key]) || empty($inputs[$key]) ) {
				$error_array[] = 'all inputs are required.';
				wp_send_json_error( $error_array );
				exit;
			}

			switch ($key) {
				case 'username':
					if( username_exists( $inputs[$key] ) ) {
						$error_array[] = 'username already exist.';
					}
					break;
					
				case 'email':
					if( !is_email( $inputs[$key] ) )
						$error_array[] = 'email is incorrect.';
					break;

				case 'password':
					if ( strlen($inputs[$key]) < 7 )
						$error_array[] = 'password must be more than 6 characters.';

					if ( $inputs[$key] !== $inputs['password-confirmation'] )
						$error_array[] = 'password and password confirmation are not the same.';

					break;
			}

		}

		if ( !empty($error_array) ) {
			wp_send_json_error( $error_array );
			exit;
		}

		list( $username, $first_name, $last_name, $email, $password, $password_confirmation ) = array(
			$inputs['username'],
			$inputs['firstname'],
			$inputs['lastname'],
			$inputs['email'],
			$inputs['password'],
			$inputs['password-confirmation'],
		);

		$userdata = array(
			'user_email' 	=> $email,
			'user_login'	=> $username,
			'display_name' 	=> "{$first_name} {$last_name}",
			'first_name'	=> $first_name,
			'last_name'		=> $last_name,
			'user_pass'		=> $password,
			// 'role'			=> 'editor'
		);

		// begin the magic :D
		$user_id = wp_insert_user( $userdata );

		if( is_wp_error($user_id) ) {
			$error_array[] = $user_id->get_error_message();
			wp_send_json_error( $error_array );
			exit;
		} else {
			$notice_note[] = 'your account has been created successfully and you will be redirect to homepage now.';

			$creds = array();
			$creds['user_login'] 	= $username;
			$creds['user_password'] = $password;
			
			$user = wp_signon( $creds, false );
			if ( is_wp_error($user) ) {
				$error_array[] = $user->get_error_message();
				wp_send_json_error( $error_array );
			}
			else {
				if( isset($_SESSION['user']) ) unset($_SESSION['user']);
				wp_send_json_success($notice_note);
			}
		}

	} else {
		wp_send_json_error( 'Who are you mother fucker !' );
		exit;
	}
}

add_action('wp_ajax_register_action', 			'register_action');
add_action('wp_ajax_nopriv_register_action', 	'register_action');