<?php
// Register Custom Taxonomy
function city() {

	$labels = array(
		'name'                       => _x( 'Cities', 'Taxonomy General Name', 'goodgame' ),
		'singular_name'              => _x( 'City', 'Taxonomy Singular Name', 'goodgame' ),
		'menu_name'                  => __( 'City', 'goodgame' ),
		'all_items'                  => __( 'All Cities', 'goodgame' ),
		'parent_item'                => __( 'Parent City', 'goodgame' ),
		'parent_item_colon'          => __( 'Parent City:', 'goodgame' ),
		'new_item_name'              => __( 'New Item City', 'goodgame' ),
		'add_new_item'               => __( 'Add New City', 'goodgame' ),
		'edit_item'                  => __( 'Edit City', 'goodgame' ),
		'update_item'                => __( 'Update City', 'goodgame' ),
		'separate_items_with_commas' => __( 'Separate Cities with commas', 'goodgame' ),
		'search_items'               => __( 'Search Cities', 'goodgame' ),
		'add_or_remove_items'        => __( 'Add or remove Cities', 'goodgame' ),
		'choose_from_most_used'      => __( 'Choose from the most used Cities', 'goodgame' ),
		'not_found'                  => __( 'Not Found', 'goodgame' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'city', array( 'playground' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'city', 0 );