<?php
// Register Custom Post Type
function playground_post_type() {

	$labels = array(
		'name'                => _x( 'playgrounds', 'Post Type General Name', 'goodgamearabia' ),
		'singular_name'       => _x( 'playground', 'Post Type Singular Name', 'goodgamearabia' ),
		'menu_name'           => __( 'playgrounds', 'goodgamearabia' ),
		'parent_item_colon'   => __( 'Parent playground:', 'goodgamearabia' ),
		'all_items'           => __( 'All playgrounds', 'goodgamearabia' ),
		'view_item'           => __( 'View playground', 'goodgamearabia' ),
		'add_new_item'        => __( 'Add New playground', 'goodgamearabia' ),
		'add_new'             => __( 'Add New', 'goodgamearabia' ),
		'edit_item'           => __( 'Edit playground', 'goodgamearabia' ),
		'update_item'         => __( 'Update playground', 'goodgamearabia' ),
		'search_items'        => __( 'Search playgrounds', 'goodgamearabia' ),
		'not_found'           => __( 'Not found', 'goodgamearabia' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'goodgamearabia' ),
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'comments', 'thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => false,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-location-alt',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'playground', $args );

}

// Hook into the 'init' action
add_action( 'init', 'playground_post_type', 0 );