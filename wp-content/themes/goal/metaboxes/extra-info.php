<?php 
	/**
	* add meta box for get videos 
	*/
	class ExtraCourtInfo {
		
		function __construct() {
			add_action( 'add_meta_boxes', array($this, 'add_meta_box') );
			add_action( 'save_post', array($this, 'save_post') );
		}

		public function add_meta_box() {
			$types = array( 'playground' );
			foreach ($types as $type) {
				add_meta_box( 
					'extra_info', 
					__('Extra Infos', 'goodgame'), 
					function($post) {

						$court_data = get_post_meta( $post->ID, 'extra_info', true );
		
						// To Send Variable
						include(
							locate_template( 'admin-parts/extra-info-meta.php' )
						);
					}, 
					$type,
					'side'
				);
			}
		}

		public function save_post($post_id) {
			$post_type = get_post_type( $post_id );
			if( $post_type == 'court') {

				if( !empty($_POST['court_data']) ) {
					$court_data = $_POST['court_data'];
					update_post_meta( $post_id, 'extra_info', $court_data );
				} else {
					delete_post_meta( $post_id, 'extra_info' );
				}
			}
		}
	}

	new ExtraCourtInfo;