<?php

add_action('parse_request', function($attrs) {
	// for reset password
	if( $attrs->request === 'login' && isset($_GET['action']) && $_GET['action'] === 'lostpassword' && !is_user_logged_in() ) {
		if( wp_verify_nonce( $_GET['nonce'], 'resetpassword' ) ) {
			global $wpdb;

			$activation_code = $_GET['user_activation_key'];

			$user_id = $wpdb->get_var(
				$wpdb->prepare("SELECT ID FROM $wpdb->users WHERE user_activation_key = %s", $activation_code)
			);

			if( !empty($user_id) ) {
				$user = get_userdata( $user_id );

				$_SESSION['user'] = $user;

				wp_redirect( get_home_url().'/reset-password' );
				exit;
			}
		}
	}

	// for profile add post
	if( $attrs->request === 'oauth2/addpost' && 'POST' == $_SERVER['REQUEST_METHOD'] && wp_verify_nonce( $_POST['_wpnonce'], 'addpost' ) ) {
		if ( !is_user_logged_in() ) 
			exit;

		global $error_array, $current_user;
		$error_array = array();

		$user_id        = $current_user->ID;

		$post_title     = strip_tags(stripcslashes($_POST['post-header']));
		$post_content   = stripcslashes($_POST['post-content']);
		$cat_id         = strip_tags(stripcslashes($_POST['post-cat']));

		// check if empty
		if (empty($post_title))     $error_array[]  = 'الرجاء إضافه عنوان.';
		if (empty($post_content))   $error_array[]  = 'الرجاء إضافه محتوى.';
		if (empty($_FILES['upload-file']['name'])) $error_array[]  = 'الرجاء إضافه صوره.';
		if (empty($cat_id))         $error_array[]  = 'الرجاء إختيار قسم.';

		if (count($error_array) == 0) {
			// These files need to be included as dependencies when on the front end.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );

			// Let WordPress handle the upload.
			// Remember, 'upload-file' is the name of our file input in our form above.
			$attachment_id = media_handle_upload( 'upload-file', 0 );

			if ( is_wp_error( $attachment_id ) ) {
	          $error_array[] = $attachment_id->get_error_message();
	        } else {
	        	wp_set_object_terms( $attachment_id, 'الملاعب', 'media_category' );

	        	// insert post
				$post_id = wp_insert_post( array(
						'post_author' => $user_id,
						'post_title'  => wp_strip_all_tags($post_title),
						'post_type'   => 'playground',
						'post_content'=> $post_content,
						'post_status' => 'publish',
					) 
				);

				if( is_wp_error($post_id) ) {
					$error_array[] = $post_id->get_error_message();
				} else {
					wp_set_object_terms( $post_id, (int)$cat_id, 'city');
		            update_post_meta($post_id,'_thumbnail_id',$attachment_id);

		            $court_data = array();
		            $court_data['space'] 		= ( isset($_POST['post-space']) && !empty($_POST['post-space']) ) ? strip_tags(stripcslashes($_POST['post-space'])) : '';
		            $court_data['price'] 		= ( isset($_POST['post-price']) && !empty($_POST['post-price']) ) ? strip_tags(stripcslashes($_POST['post-price'])) : '';
		            $court_data['phone_num'] 	= ( isset($_POST['phone_num']) && !empty($_POST['phone_num']) ) ? strip_tags(stripcslashes($_POST['phone_num'])) : '';
		            $court_data['players_num'] 	= ( isset($_POST['post-players-num']) && !empty($_POST['post-players-num']) ) ? strip_tags(stripcslashes($_POST['post-players-num'])) : '';

		            update_post_meta( $post_id, 'extra_info', $court_data );

		            $notice_note[] = "Thanks <b>".$current_user->display_name."</b> Your Court will be reviewed and then published.";
				}
	        }

	        if( count($error_array) > 0 ) {
	        	$_SESSION['message'] = array(
					'type' => 'danger',
					'data' => $error_array
				);
	        } else {
	        	$_SESSION['message'] = array(
					'type' => 'success',
					'data' => $notice_note
				);
	        }	
		} else {
			$_SESSION['message'] = array(
				'type' => 'danger',
				'data' => $error_array
			);
		}
		wp_redirect( get_home_url().'/profile/me' );
		exit;
	}

	// for social redirect
	if ( $attrs->request === 'oauth2/google' ) {
		global $theme_path;
		require_once $theme_path . DS . 'oauth2' . DS . 'google.php';
		exit;
	}
	if ( $attrs->request === 'oauth2/facebook' ) {
		global $theme_path;
		require_once $theme_path . DS . 'oauth2' . DS . 'facebook.php';
		exit;
	}

	// if ( $attrs->request === 'oauth2/twitter' ) {
	// 	global $theme_path;
	// 	require_once $theme_path . DS . 'oauth2' . DS . 'twitter.php';
	// 	exit;
	// }

});