<?php

add_filter( 'rewrite_rules_array', function ($rules) {
	$newrules = array();
	$newrules['(profile)/([\S]*)$'] = 'index.php?pagename=$matches[1]&username=$matches[2]';
	return $newrules + $rules;
}, 10, 1);

add_filter( 'query_vars', function ($vars) {
    array_push($vars, 'username');
    return $vars;
});

add_action( 'wp_loaded', function () {
	$rules = get_option( 'rewrite_rules' );

	if ( ! isset( $rules['(profile)/([\S]*)$'] ) ) {
		global $wp_rewrite;
	   	$wp_rewrite->flush_rules();
	}
});

?>