<?php  

add_action(
	'init',
	function () {
		global $pagenow;
		if ( $pagenow == 'wp-login.php' ) {
			if ( isset($_GET['action']) && $_GET['action'] == 'logout' ) {
				wp_redirect( get_home_url() . '/logout' );
			} elseif ( isset($_GET['action']) && $_GET['action'] == 'lostpassword' ) {
				wp_redirect( get_home_url() . '/login?action=\'lostpassword\'' );
			} else {
				wp_redirect( get_home_url() . '/login' );
			}
			exit;
		}
	}
);

add_action('parse_request', function ($query) {
	if ( isset($query->query_vars['name']) && $query->query_vars['name'] == 'logout' ) {
		wp_logout();

		if ( empty($_GET['r']) )
			$redirect = get_home_url();
		else 
			$redirect = $_GET['r'];

		wp_redirect( $redirect );
		exit;
	}
});


add_action('wp', function () {

	$is_login_and_logged_in 			= is_page('login') && is_user_logged_in();
	$is_register_and_logged_in 			= is_page('register') && is_user_logged_in();
	$is_lostpassword_and_logged_in 		= is_page('lostpassword') && is_user_logged_in();

	if ( $is_login_and_logged_in || $is_register_and_logged_in || $is_lostpassword_and_logged_in ) {
		wp_redirect( get_home_url() . '/profile/me' );
		exit;
	}
	
	if ( is_page('profile') && !is_user_logged_in() ) {
		wp_redirect( get_home_url() . '/login' );
		exit;
	}
	
});

?>