<?php
	global $theme_url;

	// bootstrap 
 	wp_register_style( 
 		'bootstrap-css', 
 		$theme_url . '/vendor/bootstrap/css/bootstrap.min.css'
 	);
 	wp_register_style( 
 		'bootstrap-theme-css', 
 		$theme_url . '/vendor/bootstrap/css/bootstrap-theme.min.css'
 	);

 	// font awesome
 	wp_enqueue_style( 
		'font-awesome', 
		'//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'
	);

 	// login css 
 	wp_register_style( 
 		'login-css',
 		$theme_url . '/css/login.css'	
 	);

 	// regiter css
 	wp_register_style( 
 		'register-css',
 		$theme_url . '/css/register.css'	
 	);

 	// lostpassword
 	wp_register_style( 
 		'lostpassword-css',
 		$theme_url . '/css/lostpassword.css'	
 	);

 	// resetpassword
 	wp_register_style( 
 		'resetpassword-css',
 		$theme_url . '/css/resetpassword.css'	
 	);

 	// profile
 	wp_register_style(
 		'profile-css',
 		$theme_url.'/css/profile.css'
 	);
?>