<?php  
	global $theme_url;

	// bootstrap 
	wp_register_script(
		'bootstrap-js',
		$theme_url.'/vendor/bootstrap/js/bootstrap.min.js',
		array( 'jquery' )
	);

	// login 
	wp_register_script(
		'login-js',
		$theme_url.'/js/login.js',
		array( 'jquery', 'underscore' )
	);

	// regiter
	wp_register_script(
		'register-js',
		$theme_url.'/js/register.js',
		array( 'jquery', 'underscore', 'jquery-effects-shake' )
	);	

	// lostpassword
	wp_register_script(
		'lostpassword-js',
		$theme_url.'/js/lostpassword.js',
		array( 'jquery', 'underscore' )
	);

	// Profile
	wp_register_script( 
		'profile-js', 
		$theme_url.'/js/profile.js', 
		array('jquery')
	);
?>