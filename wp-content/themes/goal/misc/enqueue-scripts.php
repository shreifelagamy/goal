<?php
	// for scripts
	require_once $theme_path . DS . 'misc/register-scripts.php';
	// for style
	require_once $theme_path . DS . 'misc/register-styles.php';

	// for enqueue scripts
	add_action('wp_enqueue_scripts', function ($paras) {

		// JS
		wp_enqueue_script( 'bootstrap-js' );
		// CSS
		wp_enqueue_style( 'bootstrap-css' );
		wp_enqueue_style( 'bootstrap-theme-css' );

		if( is_page( 'login' ) ) {
			// JS
			wp_enqueue_script( 'login-js' );
			// CSS
			wp_enqueue_style( 'login-css' );			
		}

		if( is_page( 'register' ) ) {
			// JS
			wp_enqueue_script( 'register-js' );
			// Css
			wp_enqueue_style( 'register-css' );
		}

		if( is_page( 'lostpassword' ) ) {
			// JS
			wp_enqueue_script( 'lostpassword-js' );
			// Css
			wp_enqueue_style( 'lostpassword-css' );	
		}

		if( is_page( 'reset-password' ) ) {
			// Css
			wp_enqueue_style( 'resetpassword-css' );	
		}

		if( is_page( 'profile' ) ) {
			// JS
			wp_enqueue_script( 'profile-js' );
			// CSS
			wp_enqueue_style( 'profile-css' );
		}
	});

	// for footer 
	add_action('wp_footer', function ($paras) {
		get_template_part('parts/ajax-templates/message');
		get_template_part('parts/ajax-templates/messages');
	}); 

	// for social requests
	add_action('parse_request', function ($attrs) {

		if( isset($attrs->query_vars['pagename']) &&  !$attrs->query_vars['pagename'] == 'login' )
			return;

		global $theme_path, $google_authUrl, $facebook_authUrl, $twitter_authUrl;
		set_include_path( $theme_path . DS . 'oauth2' . DS . PATH_SEPARATOR . get_include_path());

		// Google
		require_once 'Google/Client.php';
		$client_id      = '1084602166328-lhtuc6rtl2mihp7bbohllejqnjsroou6.apps.googleusercontent.com';
		$redirect_uri   = 'http://localhost/goal/oauth2/google';

		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setRedirectUri($redirect_uri);
		$client->setApprovalPrompt('auto');
		$client->setAccessType('online');
		$client->setScopes(
			'openid https://www.googleapis.com/auth/plus.login email'
		);
		$google_authUrl = $client->createAuthUrl();

		// Facebook
		if ( !isset($_SESSION['facebookLoginRedirectURL']) ) {
			require_once 'Facebook/autoload.php';
			$id 			= '803387033054873';
			$secret 		= '416239b22f2cf84f7862c7ee210be889';
			$redirect_uri	= 'http://localhost/goal/oauth2/facebook';

			Facebook\FacebookSession::setDefaultApplication($id, $secret);

			$helper = new Facebook\FacebookRedirectLoginHelper($redirect_uri);

			$facebook_authUrl = $helper->getLoginUrl(array('scope' => 'email'));
			$_SESSION['facebookLoginRedirectURL'] = $facebook_authUrl;
		} else {
			$facebook_authUrl = $_SESSION['facebookLoginRedirectURL'];
		}

		// Twitter
		// if ( !isset($_SESSION['twitterLoginRedirectURL']) ) {
		// 	require_once 'Twitter/twitteroauth.php';
		// 	require_once 'Twitter/config.php';

		// 	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
	 
		// 	/* Get temporary credentials. */
		// 	$request_token = $connection->getRequestToken(OAUTH_CALLBACK);

		// 	/* Save temporary credentials to session. */
		// 	$_SESSION['oauth_token'] = $token 	= $request_token['oauth_token'];
		// 	$_SESSION['oauth_token_secret'] 	= $request_token['oauth_token_secret'];
			
		// 	/* If last connection failed don't display authorization link. */
		// 	switch ($connection->http_code) {
		// 	  case 200:
		// 	    /* Build authorize URL and redirect user to Twitter. */
		// 	    $_SESSION['twitterLoginRedirectURL'] = $connection->getAuthorizeURL($token);
		// 	    $twitter_authUrl = $_SESSION['twitterLoginRedirectURL'];
		// 	    break;
		// 	  default:
		// 	    /* Show notification if something went wrong. */
		// 	    echo 'Could not connect to Twitter. Refresh the page or try again later. To see the error check this code '. $connection->http_code;
		// 	    exit;
		// 	}
		// } else {
		//     $twitter_authUrl = $_SESSION['twitterLoginRedirectURL'];
		// }
	}, 10, 1);

?>