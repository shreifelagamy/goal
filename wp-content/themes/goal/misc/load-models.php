<?php
add_action('after_setup_theme', 'remove_admin_bar');
	function remove_admin_bar() {
	if (!current_user_can('edit_posts')) {
		add_filter('show_admin_bar', '__return_false');
	}
}

add_action('parse_request', function($attrs) {

	if ( isset($attrs->query_vars['pagename']) && $attrs->query_vars['pagename'] === 'profile') {

		if ( !empty($attrs->query_vars['username']) ) {
			global $user;

			if ( $attrs->query_vars['username'] === 'me' ) 
				$user = wp_get_current_user();
			else
				$user = get_user_by( 'slug', $attrs->query_vars['username'] );
			
			if ( $user == null )
				$profile_exists = false;
			else
				$profile_exists = true;

		} else {
			$profile_exists = false;
		}

		if ( $profile_exists ) {
			$user = $user->data;
			$user->facebook_id 			= get_user_meta( $user->ID, 'facebook_id', true );
			$user->twitter_id 			= get_user_meta( $user->ID, 'twitter_id', true );
			$user->google_id 			= get_user_meta( $user->ID, 'google_id', true );
			$user->facebook_profile 	= get_user_meta( $user->ID, 'facebook_profile', true );
			$user->twitter_profile 		= get_user_meta( $user->ID, 'twitter_profile', true );
			$user->google_profile 		= get_user_meta( $user->ID, 'google_profile', true );
		} else {
			wp_redirect( get_home_url() . '/404' );
			exit;
		}
	}
}, 9);


/* common data */
class Router {

	public function __construct() {
		add_action('pre_get_posts', array($this, 'callAction'), 10, 1 );
	}

	public function callAction($para) {

		// get user name if logged in
		if( is_user_logged_in() ) {
			global $current_user;
			get_currentuserinfo();
			
			$para->current_user = $current_user;

			$para->profile_picture = get_template_directory_uri() . '/images/avatar.png';
			$profile = get_user_meta( $current_user->ID, 'profile', true );
			if( !empty($profile) ) {
				if( !empty($profile['image']) ) {
					$para->profile_picture = $profile['image'];
				}
			}
		} else {
			$para->profile_picture = get_template_directory_uri() . '/images/avatar.png';
		}

		if( is_page( 'profile' ) ) {
			$para->court_terms = get_terms( 'city', array('hide_empty' => true) );
		}
	}
}

new Router;