<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'goal');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ev-w[N2CV1gA/XG-~B~~is7bVmhL^rcvY1 2RuPJs5BMGJ6zUqJ4^m}{~P7-;!d/');
define('SECURE_AUTH_KEY',  'XQeMpc@oIK0Kil@/dLGiwNsIW-) YeN)&Q(22*vxwz&qpT&?%bP<U7m-`^^.&-3@');
define('LOGGED_IN_KEY',    'vYubR/RvgKfAwQ?vKeJM;d3iA.[pTg.dvh<i-J5,%eLxnhO}FE+~7|89=>_Y|E4;');
define('NONCE_KEY',        '+Cq;`p8n}5!--b>?hOvmBnZVG5SB@u!W7+bm[S 42y)<1<C+P!nLI<PDPaVs?(K9');
define('AUTH_SALT',        '#JdW>k<5{K`:_&@t&>^s|81]d5lNoaQT8#(#h|<CK6X^GA~oc/8&E[sDRdJAnb-G');
define('SECURE_AUTH_SALT', ':mQ:-` ptCa7cfr)lm3b?+-ttJe@rg;!`1<R+S,t8M+skJBOv`L@r2oNxQ=v&k:g');
define('LOGGED_IN_SALT',   '1/2XP`W*{MlTW|0f}Fs7N21qEI!mRW2n*R+qbg[$AY,W R*|nr{f(wW;lyz.Z8#$');
define('NONCE_SALT',       '2&@LM2vIqN:EM8bKUeg<q1HQt!MAU=w,>.@gn@<|-rxA[/q22]+KlK S@ga^.vPu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
